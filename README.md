INSTALL
=======

Editar el archivo docker-compose.yml con el path correcto en volumes en nginx y wordpress

Poner el dominio correcto en PROXY_PASS

Descargar la última versión de wordpress y descomprimirla en el directorio raíz:
```make download```

Crea las imagenes de wordpress, php, nginx, etc.
```docker-compose build```


Iniciar los contenedores de wordpress y sus dependencias, a partir de las imagenes creadas
```docker-compose up ```

WP-CLI
------

Para correr wp-cli dentro del contenedor usar la siguiente sintaxis:

```make wp command=COMANDO```

Ejemplo:

```
make wp command="plugin install jetpack"
docker exec wordpress-docker_wordpress_1 wp --allow-root plugin install jetpack
Installing Jetpack by WordPress.com (6.3.2)
Downloading installation package from https://downloads.wordpress.org/plugin/jetpack.6.3.2.zip...
Unpacking the package...
Installing the plugin...
Plugin installed successfully.
Success: Installed 1 of 1 plugins.

make wp command="plugin activate jetpack"
docker exec wordpress-docker_wordpress_1 wp --allow-root plugin activate jetpack
Plugin 'jetpack' activated.
Success: Activated 1 of 1 plugins.
```

TODO
----

Para restore de base de datos por el momento hacerlo asi:


```cat bonusbank.sql|docker exec -i bonusbankdocker_db_1 /usr/bin/mysql -u root --password=root wordpress ```
