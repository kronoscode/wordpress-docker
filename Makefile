download:
	curl "https://wordpress.org/latest.tar.gz" | tar -xvz
	echo 'Wordpress Downloaded'

build:
	docker-compose build

run:
	docker-compose up 

wp:
	docker exec wordpress-docker_wordpress_1 wp $(command) --allow-root
